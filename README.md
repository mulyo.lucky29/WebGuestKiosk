# Guest Kiosk
##

Guest Kiosk adalah aplikasi PWA (Progressive Web Application) yang dapat di akses via browser ataupun install di Android maupun di IOS platform. Aplikasi ini di install di table device kiosk yang di letakkan di meja receptionis.
Tujuannya adalah untuk mengurangi jumlah antrian dan mempercepat proses resgistrasi bagi tamu yang terkendala masalah koneksi ataupun tidak membawa device pribadi atau dalam kasus extreme, visitor menolak menggunakan device pribadi untuk mengakses aplikasi ini karena alasan ketidakpercayaan privacy / security. 
device id kiosk harus di enroll di aplikasi guestdesk untuk dapat di pergunakan. karena tidak semua device akan di perbolehkan mengakses layanan kiosk.

## Source File
Source file bisa akses url berikut
[https://gitlab.com/mulyo.lucky29/flutterguestkiosk](https://gitlab.com/mulyo.lucky29/flutterguestkiosk)

## Demo 
untuk demo aplikasi bisa akses url berikut
[https://webggguestkiosk.herokuapp.com/#/](https://webggguestkiosk.herokuapp.com/#/)


![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestkiosk/guest_kiosk_pict.jpg?raw=true)

## Unregistered Device
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestkiosk/not_registered.png?raw=true)
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestkiosk/guest_kiosk_unreg.jpg?raw=true)


## Device Enroll 
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestkiosk/registered_desk.png?raw=true)
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/webguestkiosk/device_rollout.png?raw=true)


## License
**Copyright 2022 Lucky Mulyo**

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
