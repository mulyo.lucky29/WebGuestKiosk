'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "assets/AssetManifest.json": "8dec401cc8659c0b21ee3bc80a883419",
"assets/assets/icons/Icon-192.png": "7ec8721be5a5abbf055f81f4cdcf8652",
"assets/assets/icons/Icon-512.png": "891f56fd3641c896d27b842652f07dc9",
"assets/assets/icons/Icon-maskable-192.png": "7ec8721be5a5abbf055f81f4cdcf8652",
"assets/assets/icons/Icon-maskable-512.png": "891f56fd3641c896d27b842652f07dc9",
"assets/assets/images/avatar.png": "1abf6ba2ee45279c7d4f994bd4ced645",
"assets/assets/images/endsession.jpg": "b34bafea14c0d1f8fc5c49555ab59c6e",
"assets/assets/images/endsession.png": "a90467619b2849cf9304dcd228fb3175",
"assets/assets/images/error.png": "792d254da434e94962e6dba593e437fd",
"assets/assets/images/expired.png": "b7fb4fae35fd9d1e21682591142a98a9",
"assets/assets/images/landing.png": "de5fd547406727601bbcfcfe9ac42527",
"assets/assets/images/logo.png": "ac243970bba6d4d361426e91a82b7b4b",
"assets/assets/images/nametag.png": "a00f93885295cee0d440ea8c2b1fed51",
"assets/assets/images/notconnect.png": "6bc0226327b106ba8df6d2797323538d",
"assets/assets/images/page401.jpg": "d87588d0996f6bcb33b316a21f07a893",
"assets/assets/images/page404.png": "05d9129a65d80a32365d60c0b98d9f86",
"assets/assets/images/powerby.png": "688ec26d22cd2cad4db4b155b5df071b",
"assets/assets/images/qrnotrecognized.png": "e51b64496eef6e9a0442d8f5e46ef01b",
"assets/assets/images/register.png": "14fc2718dcaf3c91cd725411e4d9952a",
"assets/assets/images/registerdialog.png": "902b09a653ff7f27daa3e47ff638b116",
"assets/assets/images/registersuccessdialog.png": "fe88971a6c788fb94b3598abefd1277a",
"assets/assets/images/scanme.PNG": "c1ed01f729006c52eba10dbd38febcba",
"assets/assets/images/submitDialog.png": "507c145ea3a5bf76b967b89b72e847d3",
"assets/assets/translations/en-US.json": "a8e269115f354dec80bee80225965be2",
"assets/assets/translations/id-ID.json": "e15039b96abf8af2acc8932d19765ced",
"assets/FontManifest.json": "5a32d4310a6f5d9a6b651e75ba0d7372",
"assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"assets/NOTICES": "035f9c2c14ab00638992b5639d5385be",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/packages/easy_localization/i18n/ar-DZ.json": "acc0a8eebb2fcee312764600f7cc41ec",
"assets/packages/easy_localization/i18n/ar.json": "acc0a8eebb2fcee312764600f7cc41ec",
"assets/packages/easy_localization/i18n/en-US.json": "5bd908341879a431441c8208ae30e4fd",
"assets/packages/easy_localization/i18n/en.json": "5bd908341879a431441c8208ae30e4fd",
"assets/packages/font_awesome_flutter/lib/fonts/fa-brands-400.ttf": "3241d1d9c15448a4da96df05f3292ffe",
"assets/packages/font_awesome_flutter/lib/fonts/fa-regular-400.ttf": "eaed33dc9678381a55cb5c13edaf241d",
"assets/packages/font_awesome_flutter/lib/fonts/fa-solid-900.ttf": "ffed6899ceb84c60a1efa51c809a57e4",
"assets/packages/wakelock_web/assets/no_sleep.js": "7748a45cd593f33280669b29c2c8919a",
"favicon.png": "293c7a32903f803a16c8d1f87ae09285",
"icons/Icon-192.png": "430b5cde2b4fd983198bf68a36dfe9e5",
"icons/Icon-512.png": "e847da1c510a4d73273460d7ae8ad033",
"icons/Icon-maskable-192.png": "430b5cde2b4fd983198bf68a36dfe9e5",
"icons/Icon-maskable-512.png": "e847da1c510a4d73273460d7ae8ad033",
"index.html": "27f6c83b81d1a27b7404f27bb8713781",
"/": "27f6c83b81d1a27b7404f27bb8713781",
"jsQR.min.js": "e0ebc2294c260ade0584b77f8d57bf7c",
"main.dart.js": "344de7625c952a1a4b8bd95a60ce221c",
"manifest.json": "3e7871300f2fcd52aa596ef1e5725cec",
"version.json": "58efcccf86bbb3cb74a6aad095aa5ee5"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
